# Git Demo Project
This is a Git Demo Repository to showcase the most basic git and gitlab features and workflows.

# Getting Started
- install git
- clone this repository
```sh
git clone https://gitlab.com/Brunfunstudios/git-demo-project.git
```
- listen to the presenter
- set your username and email
```sh
git config user.name "John Doe"
git config user.email "johndoe@email.xyz"
```

